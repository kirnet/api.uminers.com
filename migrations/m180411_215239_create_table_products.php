<?php

use yii\db\Migration;

class m180411_215239_create_table_products extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->execute("SET foreign_key_checks = 0;");
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'product_title' => $this->string(),
            'product_count' => $this->integer(),
            'product_price' => $this->integer(),
            'product_currency' => $this->string(),
            'product_courier' => $this->string(),
            'product_shipping_cost' => $this->string(),
            'product_shipping_date' => $this->string(),
            'date_create' => $this->integer(),
            'date_update' => $this->integer(),
            'pid' => $this->string()->notNull(),
            'active' => $this->tinyInteger()->notNull()->defaultValue('1'),
        ], $tableOptions);

        $this->createIndex('fk_products_2_idx', '{{%products}}', 'user_id');
        $this->addForeignKey('fk_products_1', '{{%products}}', 'order_id', '{{%orders}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk_products_2', '{{%products}}', 'user_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');
        $this->execute("SET foreign_key_checks = 1;");
    }

    public function down()
    {
        $this->dropTable('{{%products}}');
    }
}
