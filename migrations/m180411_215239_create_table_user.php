<?php

use yii\db\Migration;

class m180411_215239_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'login' => $this->string(),
            'name' => $this->string(),
            'surname' => $this->string(),
            'password' => $this->string(),
            'is_block' => $this->smallInteger()->notNull()->defaultValue('0'),
            'mail' => $this->text(),
            'last_visit' => $this->integer()->notNull(),
            'date_register' => $this->integer()->notNull(),
            'phone' => $this->string(),
            'role' => $this->smallInteger()->defaultValue('1'),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
