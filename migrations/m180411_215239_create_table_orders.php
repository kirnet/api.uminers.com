<?php

use yii\db\Migration;

class m180411_215239_create_table_orders extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->execute("SET foreign_key_checks = 0;");
        $this->createTable('{{%orders}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'bitmain_order_id' => $this->string(),
            'recipient_phone' => $this->string(),
            'recipient_name' => $this->string(),
            'recipient_address' => $this->text(),
            'payment_total_amount' => $this->string(),
            'payment_received_amount' => $this->string(),
            'payment_discount' => $this->string(),
            'payment_rate_btc_usd' => $this->string(),
            'payment_rate_bch_usd' => $this->string(),
            'payment_rate_ltc_usd' => $this->string(),
            'payment_rate_usd_cny' => $this->string(),
            'payment_date' => $this->string(),
            'shipment_company' => $this->string(),
            'shipment_tracking_numbers' => $this->string(),
            'shipment_date' => $this->string(),
            'order_date' => $this->string(),
            'shipment_cost' => $this->string(),
            'date_create' => $this->integer()->notNull(),
            'date_update' => $this->integer(),
            'info' => $this->json(),
            'active' => $this->tinyInteger()->notNull()->defaultValue('1'),
            'order_location' => $this->json(),
        ], $tableOptions);

        $this->createIndex('fk_user_data_1_idx', '{{%orders}}', 'user_id');
        $this->addForeignKey('fk_orders_1', '{{%orders}}', 'user_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');
        $this->execute("SET foreign_key_checks = 1;");
    }

    public function down()
    {
        $this->dropTable('{{%orders}}');
    }
}
