<?php

use yii\db\Migration;

class m180411_215239_create_table_accounts extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%accounts}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'active' => $this->tinyInteger()->notNull()->defaultValue('1'),
        ], $tableOptions);

        $this->createIndex('email_UNIQUE', '{{%accounts}}', 'email', true);
    }

    public function down()
    {
        $this->dropTable('{{%accounts}}');
    }
}
