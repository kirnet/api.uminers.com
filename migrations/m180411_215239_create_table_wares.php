<?php

use yii\db\Migration;

class m180411_215239_create_table_wares extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%wares}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'price' => $this->integer()->notNull(),
            'status' => $this->tinyInteger()->notNull(),
            'currency' => $this->string()->notNull(),
            'product_id' => $this->string()->notNull(),
            'description' => $this->string()->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%wares}}');
    }
}
