<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\Helper;
use app\components\Parser;
use app\modules\api\models\Accounts;
use app\modules\api\models\Orders;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

set_time_limit(3600);

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class CronController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
//    public function actionIndex($message = 'hello world')
//    {
//        echo $message . "\n";
//
//        return ExitCode::OK;
//    }

    /**
     * Parse all users orders
     * @throws \yii\db\Exception
     */
    public function actionParseAll()
    {
        $users = Accounts::find()->where('active=1')->all();
        if ($users) {
            (new Parser($users))->start();
        }
    }

    /**
     * Parse showcase
     */
    public function actionParseShowcase()
    {
        (new Parser())->parseShowcase();
    }

    /**
     * Add SN to orders if exists
     * @throws \yii\db\Exception
     */
    public function actionAddSnToOrders()
    {
        $orders = Helper::getOrderByCondition(
            ['user_id', 'bitmain_order_id'],
            '
                (payment_date IS NOT NULL OR payment_date !="")
                 AND JSON_EXTRACT(info, "$.sn") IS NULL
                 AND active=1'
        );
        $userIds = [];
        $userOrders = [];
        foreach ($orders as $order) {
            $userIds[] = $order['user_id'];
            $userOrders[$order['user_id']][] = $order['bitmain_order_id'];
        }
        $users = Helper::getUser(['id' => $userIds]);
        if (!$users) return false;
        $parser = new Parser($users);
        $parser->addSN($userOrders);
    }

    public function actionTest()
    {
        $selenium = new \app\components\Selenium('https://2ip.ru');
    }
}
