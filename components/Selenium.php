<?php

namespace app\components;

use Exception;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Facebook\WebDriver\WebDriverElement;
use Facebook\WebDriver\WebDriverExpectedCondition;
use Facebook\WebDriver\WebDriverKeys;
use Yii;

class Selenium
{
    /** @var  RemoteWebDriver*/
    public $driver;
    /** @var int  */
    public static $amountProxies = 0;
    /** @var int  */
    public static $currentProxyIndex = 0;

    /**
     * Selenium constructor.
     *
     * @param string $url
     */
    public function __construct($url)
    {
        $caps = DesiredCapabilities::chrome();
        $pluginForProxyLogin = '/tmp/a' . uniqid() . '.zip';
        $zip = new \ZipArchive();
        $res = $zip->open($pluginForProxyLogin, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $zip->addFile( Yii::getAlias('@app/components/seleniumProxy/manifest.json'), 'manifest.json');
        $proxy = $this->changeProxy();
        $background = file_get_contents(Yii::getAlias('@app/components/seleniumProxy/background.js'));
        $background = str_replace(['%proxy_host', '%proxy_port', '%username', '%password'], $proxy, $background);
        $zip->addFromString('background.js', $background);
        $zip->close();
        putenv('webdriver.chrome.driver=' . Yii::$app->params['chromeDriverPath']);
        $options = new ChromeOptions();
        $options->addExtensions([$pluginForProxyLogin]);
        $caps->setCapability(ChromeOptions::CAPABILITY, $options);
        unlink($pluginForProxyLogin);

        $this->driver = RemoteWebDriver::create(
            "http://localhost:4444/wd/hub",
            $caps,
            60 * 1000,
            60 * 1000
        );
        $this->driver->manage()->window()->maximize();
        $this->driver->manage()->deleteAllCookies();
        $this->driver->get($url);
    }

    /**
     * @return bool|null
     */
    public function changeProxy()
    {
        Helper::initProxy();
        if (static::$currentProxyIndex >= Helper::$proxyCounter['all']) {
            static::$currentProxyIndex = 0;
        }
        $result = [];
        $proxy = Helper::getProxyByNum(static::$currentProxyIndex);
        $result = explode(':', $proxy[1]);
        $result = array_merge($result, explode(':', $proxy[0]));

        static::$currentProxyIndex++;
        return $result;
    }

    /**
     * @param array $productInfo
     * @param array $user
     *
     * @return null|string
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    public function checkout($productInfo, $user)
    {
        $orderId = 0;
        $needNewAddress = false;

        if ($productInfo['productStatus'] == 1) {
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::className('oneclick'))
            );
            #products num
            $el = $this->driver->findElement(WebDriverBy::cssSelector('.quantity > input'));
            $el->clear();
            $el->sendKeys($productInfo['buyMaxCount']);

            $el = $this->driver->findElement(WebDriverBy::className('oneclick'));
            $el->click();
            $this->login($user);

        } elseif ($productInfo['productStatus'] == 2) {
            $this->login($user);
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('cart'))
            );

            $url = 'https://order.bitmain.com/user/orderDetails?locale=en';
            $this->driver->navigate()->to($url);
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(
                    WebDriverBy::className('submitOrderDetails')
                )
            );

            #button checkout
            $el = $this->driver->findElement(WebDriverBy::className('submitOrderDetails'));
            $el->click();
        }

        # wait popup window confirm address (.my-alert)
        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::className('my-alert')
            )
        );


//        $el = $this->driver->findElement(WebDriverBy::cssSelector('.my-alert > .content > p'));
//        $confirm = 'Please edit your address and offer a valid HK local phone number (852××××××××)';
//        if (trim($el->getAttribute('innerHTML')) === $confirm) {
//            $needNewAddress = true;
//        }

        #click in address confirm window
        $this->clickConfirmPhone();

        #wait shipping method div inputs
        try {
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(
                    WebDriverBy::cssSelector('tr.addr-item > td[title*="WALKER LOGISTICS"]')
                )
            );
        } catch(Exception $e) {
            $needNewAddress = true;
        }

        if ($needNewAddress === true) {
            $this->addShipmentAddress($user);
        }

//        $this->clickConfirmPhone();

        sleep(1);

        #click on shipping address
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('tr.addr-item > td[title*="WALKER LOGISTICS"]')
        );
        $el->click();

        $this->clickConfirmPhone();

        #wait shipping method div inputs
        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::cssSelector('.shopping-method.item .content > span:first-child')
            )
        );

        sleep(1);

        #shipping method SF
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('.shopping-method.item .content > span:first-child')
        );
        $el->click();

        #shipping method SF
//        try {
//            $el = $this->driver->findElement(
//                WebDriverBy::cssSelector('tr.addr-item > td:first-child')
//            );
//            $el->click();
//        } catch(Exception $e) {
//
//        }

        #wait popup window agree
        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::visibilityOfAnyElementLocated(
                WebDriverBy::cssSelector('.shopping-method.item .el-button.el-button--primary')
            )
        );

        #popup window agree
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('.shopping-method.item .el-button.el-button--primary')
        );
        $el->click();

        sleep(1);

        #checkbox i have read and accepted
        $el = $this->driver->findElement(WebDriverBy::className('checkbox'));
        $el->click();

        #wait popup confirm after checkBox
        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::className('confirm'))
        );

        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::visibilityOfAnyElementLocated(WebDriverBy::className('confirm'))
        );

        $el = $this->driver->findElement(WebDriverBy::className('confirm'));
        $el->click();

        sleep(1);

        #submit order
        $el = $this->driver->findElement(WebDriverBy::cssSelector('.button.ivu-btn'));
        $el->click();

        $captchaBase64 = $this->getCaptcha();
        $captcha = Helper::recognizeCaptcha($captchaBase64, ['math' => true]);
        if ($captcha) {
            $this->enterCaptcha($captcha);

            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::visibilityOfAnyElementLocated(
                    WebDriverBy::cssSelector('.verification-code .submit')
                )
            );

            sleep(15);

            $el = $this->driver->findElement(WebDriverBy::cssSelector('.submit button:first-child'));
            $el->click();

            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::visibilityOfAnyElementLocated(
                    WebDriverBy::cssSelector('.payId')
                )
            );

            $el = $this->driver->findElement(WebDriverBy::className('payId'));
            $orderId = $el->getAttribute('innerHTML');
        }
        return $orderId;
    }

    /**
     * @param $captcha
     *
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     */
    private function enterCaptcha($captcha)
    {
        sleep(1);
        $el = $this->driver->findElement(WebDriverBy::cssSelector('.verification-code input'));
        $el->sendKeys($captcha->solution->text);
        $el = $this->driver->findElement(WebDriverBy::cssSelector('.verification-code .submit'));
        $el->click();
        sleep(1);
        try {
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(
                    WebDriverBy::cssSelector('.verification-code p')
                )
            );
            $el = $this->driver->findElement(WebDriverBy::cssSelector('.verification-code p'));
        } catch(Exception $e) {
            $text = __FILE__ . __LINE__ . $e->getMessage();
            Helper::log('selenium.log', $text);
        }

        $error = $el->getAttribute('innerHTML');
        if ($error) {
            # refresh button
            $el = $this->driver->findElement(WebDriverBy::className('refresh'));
            $el->click();
            $captchaBase64 = $this->getCaptcha();
            $captcha = Helper::recognizeCaptcha($captchaBase64, ['math' => true]);
            $this->enterCaptcha($captcha);
        }
    }

    /**
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     * @return string
     */
    private function getCaptcha ()
    {
        #wait captcha
        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(
                WebDriverBy::cssSelector('.verification-code img')
            )
        );
        sleep(5);

        $this->driver->executeScript('
            let base64 = document.getElementById("base64");
            if (!base64) {
                base64 = document.createElement("input");
                document.body.appendChild(base64);
                base64.id = "base64";
            } 
            let img = document.querySelector(".verification-code img");
            let canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;
            let ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);
            let dataURL = canvas.toDataURL("image/png");
            base64.value = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            
        ');

        $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
            WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('base64'))
        );
        $element = $this->driver->findElement(WebDriverBy::id('base64'));

        return $element->getAttribute('value');
    }

    /**
     * @param array $account
     */
    public function login($account)
    {
        try {
            #wait login form
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::id('username'))
            );
            $input = $this->driver->findElement(WebDriverBy::id('username'));
            $input->sendKeys($account['email']);
            $input = $this->driver->findElement(WebDriverBy::id('password'));
            $input->sendKeys($account['password']);
            $input = $this->driver->findElement(WebDriverBy::id('submit-login'));
            $input->click();
        } catch(\Exception $e) {
            Helper::log('errors.log', $e->getMessage());
        }
    }

    private function clickConfirmPhone()
    {
        try {
            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::cssSelector('.my-alert .confirm'))
            );

            $this->driver->wait(Yii::$app->params['seleniumWait'], 1000)->until(
                WebDriverExpectedCondition::visibilityOfElementLocated(WebDriverBy::cssSelector('.my-alert .confirm'))
            );

            $el = $this->driver->findElement(WebDriverBy::cssSelector('.my-alert .confirm'));
        } catch (Exception $e) {
            Helper::log('selenium.log', $e->getMessage());
            //$el = $this->driver->findElement(WebDriverBy::cssSelector('.my-alert .confirm'));
        }
        $el->click();
    }

    protected function addShipmentAddress($user)
    {
        sleep(1);
        $el = $this->driver->findElement(WebDriverBy::cssSelector('.add-new-addr > button'));
        $el->click();
        sleep(1);

        $inputs = $this->getAddressForm();
        $inputs[0]->sendKeys($user['country']);

        sleep(1);

        #click on autocomplete menu country
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('.el-select-dropdown__item:not([style*="display: none"])')
        );
        $el->click();

        sleep(1);

        $inputs = $this->getAddressForm();
        $inputs[1]->click();

        sleep(1);

        #click State/Country
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('ul.el-scrollbar__view.el-select-dropdown__list > li.el-select-dropdown__item:not([style*="display: none"]):first-child')
        );
        $el->click();

        sleep(1);

        $inputs = $this->getAddressForm();
        $inputs[2]->click();

        sleep(1);
        #click town/city
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('div.el-select-dropdown:last-child ul.el-scrollbar__view.el-select-dropdown__list > li.el-select-dropdown__item:not([style*="display: none"])')
        );
        $el->click();

        $inputs[3]->sendKeys($user['full_name']);
        $inputs[4]->sendKeys($user['mobile']);
        $inputs[6]->sendKeys($user['zip']);

        $form = $this->driver->findElement(WebDriverBy::cssSelector('.el-form.addrAdd-en'));
        $address = $form->findElement(WebDriverBy::tagName('textarea'));
        $address->sendKeys($user['address']);

        #click save
        $el = $this->driver->findElement(
            WebDriverBy::cssSelector('button[data-v-f50154e0].el-button.el-button--primary')
        );
        $el->click();
        $this->clickConfirmPhone();
    }

    private function getAddressForm()
    {
        $form = $this->driver->findElement(WebDriverBy::cssSelector('.el-form.addrAdd-en'));
        return $form->findElements(WebDriverBy::tagName('input'));
    }

    public function __destruct()
    {
        $isDebug = Yii::$app->request->hasMethod('get') ? Yii::$app->request->get('debug') : false;

        if (!$isDebug) {
            $this->driver->close();
            $this->driver->quit();
        }
    }

}