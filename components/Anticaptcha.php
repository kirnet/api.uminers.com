<?php

namespace app\components;

use Yii;

/**
 * Class Anticaptcha
 * @package src
 */
class Anticaptcha
{
    /** @var string  */
    public static $url = 'https://api.anti-captcha.com';

    /**
     * @param $imageBase64
     * @param array $options
     *
     * @return bool|string
     */
    public static function createTask($imageBase64, $options = [])
    {
        $data = [
            'clientKey' => Yii::$app->params['antiCaptchaKey'],
            'task' => [
                'type' => 'ImageToTextTask',
                'body' => $imageBase64,
                'case' => false,
                'numeric' => true,
                'math' => (isset($options['math']) ? $options['math'] : 0),
                'minLength' => 0,
                'maxLength' => 0
            ]
        ];

        return static::jsonPostRequest('createTask', $data);
    }

    /**
     * @param $taskId
     *
     * @return mixed
     */
    public static function getTaskResult($taskId)
    {
        $data = [
            'clientKey' => Yii::$app->params['antiCaptchaKey'],
            'taskId' => $taskId
        ];
        $result = static::jsonPostRequest('getTaskResult', $data);
        if ($result && $result->status === 'processing') {
            sleep(2);
            return static::getTaskResult($taskId);
        } else {
            return $result;
        }
    }

    /**
     * @param $methodName
     * @param $postData
     *
     * @return mixed
     */
    public static function jsonPostRequest($methodName, $postData)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL, static::$url . "/$methodName");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_ENCODING,"gzip,deflate");
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST, "POST");
        $postDataEncoded = json_encode($postData);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$postDataEncoded);
        curl_setopt($ch,CURLOPT_HTTPHEADER, [
            'Content-Type: application/json; charset=utf-8',
            'Accept: application/json',
            'Content-Length: ' . strlen($postDataEncoded)
        ]);
        curl_setopt($ch,CURLOPT_TIMEOUT,30);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,30);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result);
    }
}