<?php

namespace app\components;

use Yii;

trait CounterTrait
{
    /**
     * @return array|false
     * @throws \yii\db\Exception
     */
    public static function countAllItems()
    {
        $sql = 'SELECT count(*) FROM ' . static::tableName();
        return Yii::$app->getDb()->createCommand($sql)->queryOne(\PDO::FETCH_COLUMN);
    }

    public static function countNotActiveItems()
    {
        $sql = 'SELECT count(*) FROM ' . static::tableName() . ' WHERE active=0';
        return Yii::$app->getDb()->createCommand($sql)->queryOne(\PDO::FETCH_COLUMN);
    }
}