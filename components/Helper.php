<?php
/**
 * Created by PhpStorm.
 * User: kirnet
 * Date: 07.04.18
 * Time: 22:49
 */

namespace app\components;


use Yii;

class Helper
{
    const LOG_FOLDER = '/logs/';
    public static $proxyCounter = ['all' => 0];

    /** @var array  */
    protected static $ansiCodes = [
        'off'        => 0,
        'bold'       => 1,
        'italic'     => 3,
        'underline'  => 4,
        'blink'      => 5,
        'inverse'    => 7,
        'hidden'     => 8,
        'black'      => 30,
        'red'        => 31,
        'green'      => 32,
        'yellow'     => 33,
        'blue'       => 34,
        'magenta'    => 35,
        'cyan'       => 36,
        'white'      => 37,
        'black_bg'   => 40,
        'red_bg'     => 41,
        'green_bg'   => 42,
        'yellow_bg'  => 43,
        'blue_bg'    => 44,
        'magenta_bg' => 45,
        'cyan_bg'    => 46,
        'white_bg'   => 47
    ];

    /**
     * @param $str
     * @param $color
     *
     * @return string
     */
    public static function set($str, $color)
    {
        $colorAttrs = explode("+", $color);
        $ansiStr = "";
        foreach ($colorAttrs as $attr) {
            $ansiStr .= "\033[" . self::$ansiCodes[$attr] . "m";
        }
        $ansiStr .= $str . "\033[" . self::$ansiCodes["off"] . "m";
        return $ansiStr;
    }

    /**
     * @return array
     */
    public static function YesNo()
    {
        return [Yii::t('app', 'No'), Yii::t('app', 'Yes')];
    }

    /**
     * @param $fullText
     * @param $searchRegexp
     * @param $color
     *
     * @return null|string|string[]
     */
    public static function replace($fullText, $searchRegexp, $color)
    {
        $newText = preg_replace_callback(
            "/($searchRegexp)/",
            function ($matches) use ($color) {
                return static::set($matches[1], $color);
            },
            $fullText
        );
        return is_null($newText) ? $fullText : $newText;
    }

    /**
     * @param \Reflection $reflection
     *
     * @return string
     */
    public static function getMethodDoc($reflection)
    {
        $comment = $reflection->getDocComment();
        $comment = explode("\n", $comment);
        if (isset($comment[1])) {
            return trim($comment[1], "\t *");
        }
        return '';
    }

    /**
     * @param $message
     *
     * @return bool|string
     */
    public static function sendToTelegram($message)
    {
        $token = Yii::$app->params['telegramBotToken'];
        $chatId = Yii::$app->params['telegramChatId'];
        $message = urlencode($message);
        $sUrl ="https://api.telegram.org/bot{$token}/sendMessage?chat_id={$chatId}&parse_mode=html&text={$message}";
        return file_get_contents($sUrl);
    }

    /**
     * @param $param
     *
     * @return string
     */
    public static function getParam($param)
    {
        return isset($_GET[$param]) ? $_GET[$param] : '';
    }

    /**
     * @param $json
     *
     * @return array
     */
    public static function jsonParse($json)
    {
        try {
            $data = json_decode($json, true);
        } catch(\Exception $e) {
            static::log('json_parse.log', $e->getMessage());
            return [];
        }
        return $data['data'];
    }

    /**
     * @param string $file
     * @param string $data
     * @param string $currAccount
     * @param bool $append
     */
    public static function log($file, $data, $currAccount = '', $append = true)
    {
        $file = Yii::getAlias('@runtime') . static::LOG_FOLDER . $file;
        $now = date('d.m.Y H:i:s');
        if ($currAccount) {
            $currAccount .= "\n";
        }
        $data = "{$currAccount}{$now}\n$data\n\n";
        $flag = $append ? FILE_APPEND : 0;
        file_put_contents($file, $data, $flag);
        chmod($file, 0777);
    }

    /**
     * @param string $file
     *
     * @return bool
     */
    public static function deleteLog($file)
    {
        $file = Yii::getAlias('@runtime') . static::LOG_FOLDER . $file;
        if (file_exists($file)) {
            return unlink($file);
        }
    }

    /**
     * @param $email
     *
     * @return array|false
     * @throws \yii\db\Exception
     */
    public static function checkEmailExists($email)
    {
        return Yii::$app->getDb()->createCommand("SELECT id FROM accounts WHERE email='{$email}'")->queryOne();
    }

    /**
     * @param string $base64Image
     * @param array $options
     *
     * @return mixed
     */
    public static function recognizeCaptcha($base64Image, $options = [])
    {
        $result = Anticaptcha::createTask($base64Image, $options);
        if ($result && $result->errorId == 0) {
            return Anticaptcha::getTaskResult($result->taskId);
        }
    }

    /**
     * @param array $field
     * @param int $active
     * $active 0 - not active users, 1 - active users, 2 - all users
     *
     * @return array|mixed
     * @throws \yii\db\Exception
     */
    public static function getUser($field = [], $active = 1)
    {
        $isOne = true;
        $key = key($field);
        if ($key === 'email' && !filter_var($field[$key], FILTER_VALIDATE_EMAIL)) {
            return [];
        }

        if (is_array($field[$key])) {
            $isOne = false;
            $in = "'" . implode("','", $field[$key]) . "'";
            $where = "{$key} IN ({$in})";
        } else {
            $where = "{$key}='{$field[$key]}'";
        }

        if ($active !== 2) {
            $where .= " AND active={$active}";
        }

        $sql = "SELECT * FROM accounts WHERE {$where}";
        $users = Yii::$app->getDb()->createCommand($sql);
        if ($isOne) {
            return $users->queryOne();
        }
        return $users->queryAll();
    }

    /**
     * Get orders by conditions (if conditions is empty get all)
     *
     * @param array $fields
     * @param string $condition
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public static function getOrderByCondition($fields = [], $condition = '')
    {
        if ($fields) {
            $fields = implode(',', $fields);
        } else {
            $fields = '*';
        }
        $sql = "SELECT {$fields} FROM orders";
        if ($condition) {
            $sql .= " WHERE {$condition}";
        }
        return Yii::$app->getDb()->createCommand($sql)->queryAll();
    }

    /**
     * @return array
     */
    public static function initProxy()
    {
        if (static::$proxyCounter['all'] > 0) {
            return static::$proxyCounter;
        }
        $proxies = Yii::$app->params['proxy'];
        foreach ($proxies as $loginPass => $proxy) {
            $c = count($proxy);
            static::$proxyCounter['all'] += $c;
            static::$proxyCounter[$loginPass] = $c;
        }
        return static::$proxyCounter;
    }

    /**
     * @param integer $num
     *
     * @return mixed
     */
    public static function getProxyByNum($num = 0)
    {
        $count = 0;
        $prevNum = 0;
        if (static::$proxyCounter['all'] === 0) {
            static::initProxy();
        }
        if ($num > static::$proxyCounter['all']) {
            $num = 0;
        }
        foreach (static::$proxyCounter as $loginPass => $countProxy) {
            if ($loginPass === 'all') {
                continue;
            }
            $count += $countProxy;
            if ($num < $count) {
                return [$loginPass, Yii::$app->params['proxy'][$loginPass][$num - $prevNum]];
            }
            $prevNum = $countProxy;
        }
        return false;
    }

    public static function getAvailableCountries()
    {
        return [
            "Afghanistan",
            "Aland Islands",
            "Albania",
            "Algeria",
            "American Samoa",
            "Andorra",
            "Angola",
            "Anguilla",
            "Antigua and Barbuda",
            "Argentina",
            "Armenia",
            "Aruba",
            "Australia",
            "Austria",
            "Azerbaijan",
            "Bahamas",
            "Bahrain",
            "Bangladesh",
            "Barbados",
            "Belgium",
            "Belize",
            "Benin",
            "Bermuda",
            "Bhutan",
            "Bolivia",
            "Bosnia and Herzegovina",
            "Botswana",
            "Brazil",
            "British Virgin Islands",
            "Brunei",
            "Bulgaria",
            "Burkina Faso",
            "Burundi",
            "Cambodia",
            "Cameroon",
            "Canada",
            "Cape Verde",
            "Caribbean Netherlands",
            "Cayman Islands",
            "Central African Republic",
            "Chad",
            "Chile",
            "China",
            "Colombia",
            "Comoros",
            "Congo",
            "Congo (Congo-Kinshasa)",
            "Cook Islands",
            "Costa Rica",
            "Cote D'Ivoire",
            "Croatia",
            "Cura ao",
            "Cyprus",
            "Czech Republic",
            "Denmark",
            "Djibouti",
            "Dominica",
            "Dominican Republic",
            "Ecuador",
            "Egypt",
            "El Salvador",
            "Equatorial Guinea",
            "Eritrea",
            "Estonia",
            "Ethiopia",
            "Falkland Islands",
            "Faroe Islands",
            "Fiji",
            "Finland",
            "France",
            "French Guiana",
            "French Polynesia",
            "Gabon",
            "Gambia",
            "Georgia",
            "Germany",
            "Ghana",
            "Gibraltar",
            "Greece",
            "Greenland",
            "Grenada",
            "Guadeloupe",
            "Guam",
            "Guatemala",
            "Guernsey",
            "Guinea",
            "Guinea-Bissau",
            "Guyana",
            "Haiti",
            "Honduras",
            "Hungary",
            "Iceland",
            "Indonesia",
            "Iraq",
            "Ireland",
            "Israel",
            "Italy",
            "Jamaica",
            "Japan",
            "Jersey",
            "Jordan",
            "Kazakhstan",
            "Kenya",
            "Kiribati",
            "Kosovo",
            "Kuwait",
            "Kyrgyzstan",
            "Laos",
            "Latvia",
            "Lebanon",
            "Lesotho",
            "Liberia",
            "Libya",
            "Liechtenstein",
            "Lithuania",
            "Luxembourg",
            "Macedonia",
            "Madagascar",
            "Malawi",
            "Malaysia",
            "Maldives",
            "Mali",
            "Malta",
            "Marshall Islands",
            "Martinique",
            "Mauritania",
            "Mauritius",
            "Mayotte",
            "Mexico",
            "Micronesia",
            "Moldova",
            "Monaco",
            "Mongolia",
            "Montenegro",
            "Montserrat",
            "Morocco",
            "Mozambique",
            "Myanmar",
            "Namibia",
            "Nauru",
            "Nepal",
            "Netherlands",
            "Netherlands Antilles",
            "New Caledonia",
            "New Zealand",
            "Nicaragua",
            "Niger",
            "Nigeria",
            "Niue",
            "Norfolk Island",
            "Norway",
            "Oman",
            "Pakistan",
            "Palau",
            "Panama",
            "Papua New Guinea",
            "Paraguay",
            "Peru",
            "Philippines",
            "Poland",
            "Portugal",
            "PuertoRico",
            "Qatar",
            "Reunion",
            "Romania",
            "Rwanda",
            "Saint Barthélemy",
            "Saint Helena",
            "Saint Kitts-Nevis",
            "Saint Lucia",
            "Saint Martin",
            "Saint Martin (Dutch part)",
            "Saint Martin (French part)",
            "Saint Vincent and the Grenadines",
            "Saipan lsland",
            "San Marino",
            "Sao Tome and Principe",
            "Saudi Arabia",
            "Senegal",
            "Serbia",
            "Seychelles",
            "Sierra Leone",
            "Singapore",
            "Slovakia",
            "Slovenia",
            "Solomon Islands",
            "Somalia",
            "South Africa",
            "South Korea",
            "South Sudan",
            "Spain",
            "Sri Lanka",
            "St. barthelemy",
            "ST. EUSTATIUS",
            "Suriname",
            "Swaziland",
            "Sweden",
            "Switzerland",
            "Tahiti",
            "Tanzania",
            "Thailand",
            "TheUnitedStatesVirgin",
            "Timor-Leste",
            "Togo",
            "Tonga",
            "Trinidad and Tobago",
            "Tunisia",
            "Turks and Caicos Islands",
            "Tuvalu",
            "Uganda",
            "United Arab Emirates",
            "United Kingdom",
            "United States",
            "Uruguay",
            "Uzbekistan",
            "Vanuatu",
            "Vatican",
            "Vatican",
            "Venezuela",
            "Vietnam",
            "Wallis and Futuna",
            "Yemen",
            "Zambia",
            "Zimbabwe"
        ];
    }
}