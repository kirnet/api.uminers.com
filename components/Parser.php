<?php

namespace app\components;

use app\modules\api\models\Orders;
use app\modules\api\models\Products;
use app\modules\api\models\Wares;
use phpQuery;
use Yii;
use yii\web\Response;

/**
 * Class Parser
 */
class Parser
{
    const HTML_FILE_PATH = '@runtime/orders_html/*';
    const INTERRUPT_USER = 'interruptUser.txt';

    /** @var array */
    private $accounts;

    /** @var string */
    private $url = 'https://bitmain.com';

    /** @var string */
    public $urlLogin = 'https://passport.bitmain.com/login';

    /** @var array  */
    private $urlOrders = 'https://shop.bitmain.com/user/userOrderShow.htm?type=0';

    /** @var resource */
    private $curl;

    /** @var string */
    private $xsrf;

    /** @var string */
    private $lt;

    /** @var integer */
    private $userId;

    /** @var integer  */
    private $curlExecCounter = 0;

    /** @var integer  */
    private $currentProxyIndex = 0;
    /** @var string */
    private $captcha;

    /** @var array  */
    private $ordersDates = [];

    /**
     * Parser constructor.
     *
     * @param $accounts
     */
    public function __construct($accounts = [])
    {
        $this->accounts = $accounts;
        $this->curlInit();
    }

    public function parseShowcase()
    {
        $url = 'https://shop-product.bitmain.com/api/product/getProducts?lang=en';
        $json = json_decode($this->getContent($url), true);
        if (!$json) {
            return false;
        }
        $productIds = [];
        foreach ($json['data'] as $product) {
            $productIds[] = $product['productId'];
        }
        $products = Wares::getWaresByProductId($productIds);
        //-----------------------------------------------------------

        $messages = [];
        foreach ($json['data'] as $newProduct) {
            if (isset($products[$newProduct['productId']])) {
                $sql = '';
                $setData = '';
                if ($products[$newProduct['productId']]['status'] != $newProduct['productStatus']) {
                    $setData = "status='{$newProduct['productStatus']}', ";
                    if ($newProduct['productStatus'] < 3) {
                        $messages[] = "{$newProduct['name']} - {$newProduct['price']} {$newProduct['currency']}\r\n";
                    }
                }
                if ($products[$newProduct['productId']]['price'] != $newProduct['price']) {
                    $setData .= "price='{$newProduct['productStatus']}', ";
                }
                if ($products[$newProduct['productId']]['currency'] != $newProduct['currency']) {
                    $setData .= "currency='{$newProduct['currency']}', ";
                }
                if ($products[$newProduct['productId']]['description'] != $newProduct['description']) {
                    $setData .= "description='{$newProduct['description']}', ";
                }
                if ($setData) {
                    $sql = 'UPDATE wares SET ';
                    $sql .= substr($setData, 0, -2) . "WHERE product_id='{$newProduct['productId']}'";
                }

            } else {
                $sql = "INSERT INTO wares 
                  (name, price, status, currency, product_id, description)
                  VALUES(
                    '{$newProduct['name']}', 
                    '{$newProduct['price']}', 
                    '{$newProduct['productStatus']}',
                    '{$newProduct['currency']}',
                    '{$newProduct['productId']}',
                    '{$newProduct['description']}'
                  )";
                if ($newProduct['productStatus'] < 3) {
                    $messages[] = "{$newProduct['name']} - {$newProduct['price']} {$newProduct['currency']} \r\n";
                }
            }
            if (!empty($sql)) {
                $result = Yii::$app->getDb()->createCommand($sql)->execute();
                //Helper::log('showcase.log', $result);
            }
        }
        if ($messages) {
            $message = "Products:  \r\n";
            foreach ($messages as $mess) {
                $message .= $mess ;
            }
            $message = substr($message, 0, -1) . "\r\n is available to buy";
            Helper::sendToTelegram($message);
        }
    }

    /**
     * @return void
     */
    public function setTokens()
    {
        $html = $this->getContent($this->urlLogin);
        $this->xsrf = $this->getXsrf($html);
        $this->lt = $this->getLt($html);
        $this->captcha = $this->getCaptcha($html);
    }

    /**
     * Initialize curl
     */
    public function curlInit()
    {
        if (!$this->curl) {
            $this->curl = curl_init();
            $userAgent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36';
            curl_setopt($this->curl, CURLOPT_USERAGENT, $userAgent);
            curl_setopt($this->curl, CURLOPT_HEADER, false);
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($this->curl, CURLOPT_POST, false);
            curl_setopt($this->curl, CURLOPT_COOKIEJAR,  '-');
            curl_setopt($this->curl, CURLOPT_TIMEOUT,30);
            curl_setopt($this->curl, CURLOPT_CONNECTTIMEOUT,  30);

//            $f = fopen('../data/logs/request.txt', 'w');
//            curl_setopt($this->curl, CURLOPT_VERBOSE, true);
//            curl_setopt($this->curl, CURLOPT_STDERR, $f);
//            fclose($f);
        }
    }

    /**
     * @return bool|null
     */
    public function changeCurlProxy()
    {
        if (Helper::$proxyCounter['all'] === 0) return false;
        if ($this->currentProxyIndex >= Helper::$proxyCounter['all']) {
            $this->currentProxyIndex = 0;
        }
        $proxy = Helper::getProxyByNum($this->currentProxyIndex);
        $loginPass = $proxy[0];
        curl_setopt($this->curl, CURLOPT_PROXY, $proxy[1]);
        curl_setopt($this->curl, CURLOPT_PROXYUSERPWD, $loginPass);
        $this->currentProxyIndex++;
    }

    /**
     * @param string $url
     *
     * @return mixed
     */
    public function getContent($url)
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
        return curl_exec($this->curl);
    }

    /**
     * @param array $account
     *
     * @return mixed
     */
    public function login($account)
    {
        $this->userId = $account['id'];
        $postFields = [
            'username' => $account['email'],
            'password' => $account['password'],
            '_xsrf' => $this->xsrf,
            'lt' => $this->lt
        ];
        if ($this->captcha) {
            Helper::log('captcha.log', 'send captcha', $account['email']);
            $captcha = $this->recognizeCaptcha();
            Helper::log('captcha.log', 'recognized captcha - ' . json_encode($captcha), $account['email']);
            if ($captcha) {
                $postFields['captcha'] = $captcha->solution->text;
                $postFields['captchaId'] = $this->captcha;
            }
            Helper::log('login.log', json_encode($postFields) . '|' . json_encode($captcha), $account['email']);
        }
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postFields);
        $result = curl_exec($this->curl);
        if (strpos($result, 'Login Success!') !== false) {
            return true;
        }
        return false;
    }

    /**
     * @param string $url
     * @param array $data
     * @param string $method
     * @param bool $sendJson
     * @param array $headers
     *
     * @return mixed
     */
    public function sendRequest($url, $data, $method = 'POST', $sendJson = false, $headers = [])
    {
        if ($sendJson) {
            $data = json_encode($data);
            $headers += [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data),
                'Upgrade-Insecure-Requests: 1'
            ];

        } else {
            $data = http_build_query($data);
        }
        if ($headers) {
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, $method);
//        if ($method == 'OPTIONS') {
//
//        }
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($this->curl);
        curl_setopt($this->curl, CURLOPT_HTTPGET, true);
        return $result;
    }

    /**
     * @return mixed
     */
    private function recognizeCaptcha()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $captchaImage = base64_encode(file_get_contents(
            'https://passport.bitmain.com/captcha/' . $this->captcha . '.png'
        ));
        $result = Anticaptcha::createTask($captchaImage);
        if ($result && $result->errorId == 0) {
            return Anticaptcha::getTaskResult($result->taskId);
        }
    }

    /**
     * @param $html
     *
     * @return \phpQueryObject|\QueryTemplatesParse|\QueryTemplatesSource|\QueryTemplatesSourceQuery
     */
    public function getLinksDetails($html)
    {
        $document = phpQuery::newDocument($html);
        return $document->find('.th-order-info div a:not(".lz-pay")');
    }

    /**
     * @param string $html
     *
     * @return array
     */
    public function getOrdersPages($html)
    {
        $pages = [];
        $document = phpQuery::newDocument($html);
        $el = $document->find('.btn-skin-c.btn-4-21:last');
        $href = $el->attr('onclick');
        preg_match('/tp=(\d+)/', $href, $match);
        if (isset($match[1]) && $match[1] > 1) {
            for ($i = 1; $i <= $match[1]; $i++) {
                if ($i > 1) {
                    $pages[] = "https://shop.bitmain.com/user/userOrderShow.htm?tp={$i}&type=&status=";
                }
            }
        }
        return $pages;
    }

    /**
     * @param $links
     */
    public function crawlOrdersLinks($links)
    {
        $urlPath = 'https://shop.bitmain.com/user/';
        foreach ($links as $link) {
            $pq = pq($link);
            $url = $urlPath .  $pq->attr('href');
            $orderId = explode('=', $url)[1];
            $html = $this->getContent($url);
            if (!is_dir(Yii::getAlias('@runtime/orders_html'))) {
                mkdir(Yii::getAlias('@runtime/orders_html'));
            }
            $filename = Yii::getAlias('@runtime/orders_html/') . $orderId;
            file_put_contents($filename, $url . "\n\n" . $html);
            chmod($filename, 0777);
        }
    }

    /**
     * @param $links
     */
    public function crawlMultiOrdersLinks($links)
    {
        $urlPath = 'https://shop.bitmain.com/user/';
        $urls = [];
        $handlers = [];
        foreach ($links as $link) {
            $pq = pq($link);
            $url = $urlPath .  $pq->attr('href');
            $orderId = explode('=', $url)[1];
            $urls[$orderId] = $url;
        }

        foreach ($urls as $orderId => $url) {
            $handlers[$orderId] = curl_copy_handle($this->curl);
            curl_setopt($handlers[$orderId], CURLOPT_URL, $url);
        }

        $mh = curl_multi_init();

        foreach ($handlers as $handler) {
            curl_multi_add_handle($mh, $handler);
        }

        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);

        foreach ($handlers as $orderId => $handler) {
            $filename = __DIR__ . '/../data/orders_html/' . $orderId;
            file_put_contents($filename, curl_multi_getcontent($handler));
            chmod($filename, 0777);
            curl_multi_remove_handle($mh, $handler);
        }
        curl_multi_close($mh);
    }

    /**
     * @param string $html
     *
     * @return string
     */
    public function getOrderUrl($html)
    {
        return substr($html, 0, strpos($html, "\n"));
    }

    /**
     * @param $html
     *
     * @return array
     */
    public function parseOrder($html)
    {
        $orderUri = $this->getOrderUrl($html);
        $data = ['user_id' => $this->userId];
        $document = phpQuery::newDocument($html);

        $el = $document->find('#subId');
        $data['bitmain_order_id'] = trim($el->html());

        if (!$data['bitmain_order_id']) {
            return [];
        }

        $el = $document->find('.order-pay-list .tableOrder tr:first td:last');
        $data['recipient_name'] = $el->html();

        $el = $document->find('.order-pay-list .tableOrder tr:eq(1) td:last');
        $data['recipient_phone'] = $el->html();

        $el = $document->find('.order-pay-list .tableOrder tr:eq(2) td:last');
        $data['recipient_address'] = trim($el->html());

        $el = $document->find('.line:eq(1) .tableOrder tr:first td:eq(1) span');
        $data['payment_total_amount'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder tr:eq(1) td:eq(1) span');
        $data['payment_received_amount'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder tr:eq(2) td:eq(1) span');
        $data['payment_discount'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder tr:eq(3) td:eq(1) span');
        $data['payment_rate_btc_usd'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder tr:eq(4) td:eq(1) span');
        $data['payment_rate_bch_usd'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder tr:eq(5) td:eq(1) span');
        $data['payment_rate_ltc_usd'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder tr:eq(6) td:eq(1) span');
        $data['payment_rate_usd_cny'] = $el->html();

        $el = $document->find('.line:eq(1) .tableOrder>td:eq(1) span');
        $data['payment_date'] = $el->html();

        $el = $document->find('#billName');
        $data['shipment_company'] = $el->html();

        $el = $document->find('#toUserExpress');
        $data['shipment_tracking_numbers'] = $el->html();

        $el = $document->find('.line:eq(2) .tableOrder tr:eq(2) td:eq(1)');
        $data['shipment_date'] = $el->html();

        $el = $document->find('.line:eq(2) .tableOrder tr:eq(3) td:eq(1)');
        $data['shipment_cost'] = $el->html();

        $el = $document->find('#toUserExpress');
        $bIds = trim($el->html());
        $locations = [];
        if ($bIds) {
            $bIds = explode(',', $bIds);
            foreach ($bIds as $userBillId) {
                $info = json_decode($this->getOrderDetails($userBillId, $data['bitmain_order_id'], $orderUri), true);
                if (!$info || $info['Message'] == 'noresult' || $info['IsError'] == true) {
                    continue;
                }
                $locations[$userBillId] = $info['Data'];
            }
        }
        $data['order_location'] = json_encode($locations);
        $data['order_date'] = $this->ordersDates[$data['bitmain_order_id']];

        return $data;
    }

    /**
     * @param $userBillId
     * @param $sid
     * @param $orderUri
     *
     * @return mixed
     */
    private function getOrderDetails($userBillId, $sid, $orderUri)
    {
        $urlOrderDetail = 'https://shop.bitmain.com/user/userOrderDetailShow.htm?m=selectExpress';
        $postData = [
            'sid' => $sid,
            'userBillId' => $userBillId,
        ];
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($this->curl, CURLOPT_CUSTOMREQUEST, 'POST');
        return $this->getContent($urlOrderDetail);
    }

    /**
     * @param string $html
     * @param string $orderId
     * @return array
     */
    public function parseProducts($html, $orderId)
    {
        $document = phpQuery::newDocument($html);
        $data = [];
        $el = $document->find('.tbl-outer:first tr');
        $productsCount = $el->length ;

        for ($i = 1; $i < $productsCount; $i++) {
            $product = ['user_id' => $this->userId, 'order_id' => $orderId];
            $el = $document->find(".tbl-outer:first tr:eq({$i}) td:eq(1) a");
            $title = explode('<br>', $el->html());
            $product['product_title'] = strip_tags($title[0]);
            $product['product_shipping_date'] = isset($title[1]) ? $title[1] : null;

            $product['pid'] = explode('=', $el->attr('href'))[1];
            $el = $document->find(".tbl-outer tr:eq({$i}) td:eq(3)");
            $product['product_count'] = (int) $el->html();

            $el = $document->find(".tbl-outer tr:eq({$i}) td:eq(2)");
            $price = explode("\n", $el->html());
            $product['product_price'] = $price[0];
            $product['product_currency'] = $price[1];

            $el = $document->find(".tbl-outer tr:eq({$i}) td:eq(4)");
            $product['product_courier'] = $el->html();

            $el = $document->find(".tbl-outer tr:eq({$i}) td:eq(5)");
            $product['product_shipping_cost'] = $el->html();

            $data[$product['pid']] = $product;
        }

        return $data;
    }

    /**
     * @param string $table
     * @param array $data
     * @param bool $multi
     *
     * @return array|int
     * @throws \yii\db\Exception
     */
    public function save($table, $data, $multi = false)
    {
        $counter = 0;
        $now = time();
        if ($multi) {
            $ids = [];
            foreach ($data as $val) {
                $ids[] = $this->save($table, $val);
            }
            return $ids;

        } else {
            $order = $table == 'orders' ? Orders::getOrderByBitmainId($data['bitmain_order_id']) : [];
            if ($order) {
                $changes = $this->createChangesLog($order->getAttributes(), $data);
                if ($changes) {
                    $fields = '';
                    foreach ($changes as $key => $val) {
                        $fields .= "{$key}='{$data[$key]}', ";
                    }
                    $fields .= 'date_update=' . $now;
                    $sql = "UPDATE {$table} SET {$fields} WHERE bitmain_order_id='{$data['bitmain_order_id']}'";
                } else {
                    return $order['id'];
                }
            } else {
                $sql = "INSERT INTO {$table} (";
                $values = 'VALUES (';
                $data['date_create'] = $now;
                foreach ($data as $key => $val) {
                    if ($val) {
                        $sql .= $key . ', ';
                        $values .=  "'" . trim($val) . "'" . ', ';
                        $counter++;
                    }
                }
                if ($counter < 2) return 0;
                $sql = substr($sql, 0,-2) . ') ' . substr($values, 0, -2) . ')';
            }
        }
        if ($sql && Yii::$app->getDb()->createCommand($sql)->execute()) {
            return Yii::$app->getDb()->lastInsertID;
        }
        return 0;
    }

    /**
     * @param $data
     * @param $newData
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function createChangesLog($data, $newData)
    {
        $newData = array_map(function($el) {
            return trim($el);
        }, $newData);
        unset($data['order_location']);
        $diff = array_diff_assoc($data, $newData);
        unset(
            $diff['id'],
            $diff['date_create'],
            $diff['date_update'],
            $diff['info'],
            $diff['active'],
            $diff['order_location']
        );
        if ($diff) {
            $now = date('d.m.Y H:i:s');
            $info = json_decode($data['info'], true);
            if (!$info) {
                $info = ['changes' => []];
            }
            foreach ($diff as $key => $val) {
                $info['changes'][$now][$key] = [
                    'old' => $val,
                    'new' => $newData[$key]
                ];
            }
            $info = json_encode($info);
            $sql = "UPDATE orders set info='{$info}' WHERE bitmain_order_id='{$data['bitmain_order_id']}'";
            Yii::$app->getDb()->createCommand($sql)->execute();
        }
        return $diff;
    }

    /**
     * Start parse all
     * @throws \yii\db\Exception
     */
    public function start()
    {
        if (!$this->accounts) die('No users');
        $interruptUser = $this->getInterruptUser();
        foreach ($this->accounts as $account) {
            if ($interruptUser) {
                if ($account->email != $interruptUser) {
                    continue;
                } else {
                    $interruptUser = false;
                }
            }

            Helper::log(static::INTERRUPT_USER, '', $account['email'], false);
            $this->curlInit();
            $this->setTokens();
            if ($this->curlExecCounter >= Yii::$app->params['curlExecLimit']) {
                $this->changeCurlProxy();
            }

            if ($this->login($account)) {
                Helper::log('login.log', 'login success!', $account['email']);
                $html = $this->getContent($this->urlOrders);
                $pages = $this->getOrdersPages($html);
                $links = $this->getLinksDetails($html);
                $this->setOrdersDates($html);
                if ($pages && isset($links->elements)) {
                    foreach ($pages as $page) {
                        $html = $this->getContent($page);
                        $this->setOrdersDates($html);
                        $tmpLinks = $this->getLinksDetails($html);
                        $links->elements = array_merge($links->elements, $tmpLinks->elements);
                    }
                }

                $this->clearOrdersHtml();
                $this->crawlOrdersLinks($links);
                foreach (glob(Yii::getAlias(static::HTML_FILE_PATH)) as $file) {
                    $html = file_get_contents($file);
                    $data = $this->parseOrder($html);
                    $orderId = $this->save('orders', $data);
                    if ($orderId) {
                        $data = $this->parseProducts($html, $orderId);
                        $data = Products::filterProductsExists($data, $orderId);
                        $Ids = $this->save('products', $data, true);
                    }
                }
            } else {
                Helper::log('login.log', 'login failure', $account['email']);
            }
            curl_close($this->curl);
            $this->curl = false;
            $this->curlExecCounter++;
            Helper::deleteLog(static::INTERRUPT_USER);
        }
    }

    /**
     * @param $id
     *
     * @return array|int
     * @throws \yii\db\Exception
     */
    public function getOrderById($id)
    {
        $this->setTokens();
        $orderId = 0;

        if ($this->login($this->accounts)) {
            $html = $this->getContent($this->urlOrders);
            $pages = $this->getOrdersPages($html);
            $orderLink = $this->findOrderById($html, $id);
            if (!$orderLink) {
                foreach ($pages as $url) {
                    $html = $this->getContent($url);
                    $orderLink = $this->findOrderById($html, $id);
                    if ($orderLink) {
                        break;
                    }

                }
            }

            $html = $this->getContent($orderLink);
            $data = $this->parseOrder($html);
            if ($data) {
                $orderId = $this->save('orders', $data);
                if ($orderId) {
                    $data = $this->parseProducts($html, $orderId);
                    $data = Products::filterProductsExists($data, $orderId);
                    $Ids = $this->save('products', $data, true);
                }
            }
            return $orderId;
        }
    }

    /**
     * @param string $html
     * @param string $orderId
     *
     * @return string
     */
    private function findOrderById($html, $orderId)
    {
        $urlPath = 'https://shop.bitmain.com/user/';
        $document = phpQuery::newDocument($html);
        $el = $document->find('.th-order-info a');

        foreach ($el->elements as $element) {
            $pq = pq($element);
            $order = $pq->parents()->eq(1)->prev()->find('span')->html();
            $date = substr($order, 0, 19);
            $order = substr($order, 30);
            if ($order == $orderId) {
                $this->ordersDates[$order] = $date;
                return $urlPath .  $pq->attr('href');
            }
        }
        return '';
    }

    /**
     * @param $html
     */
    private function setOrdersDates($html)
    {
        $document = phpQuery::newDocument($html);
        $el = $document->find('.th-order-info a');

        foreach ($el->elements as $element) {
            $pq = pq($element);
            $order = $pq->parents()->eq(1)->prev()->find('span')->html();
            $date = substr($order, 0, 19);
            $order = substr($order, 30);
            $this->ordersDates[$order] = $date;
        }
    }

    /**
     * Clear tmp html files
     */
    public function clearOrdersHtml()
    {
        foreach (glob(Yii::getAlias(static::HTML_FILE_PATH)) as $file) {
            unlink($file);
        }
    }

    /**
     * @param $html
     *
     * @return String
     */
    private function getCaptcha($html)
    {
        $document = phpQuery::newDocument($html);
        $el = $document->find('input[name="captchaId"]');
        return $el->val();
    }

    /**
     * @param $html
     *
     * @return string
     */
    private function getXsrf($html)
    {
        preg_match('/_xsrf"\svalue="(.*)"/Uis', $html, $match);
        return isset($match[1]) ? $match[1] : '';
    }

    /**
     * @param $html
     *
     * @return string
     */
    private function getLt($html)
    {
        preg_match('/"lt"\svalue="(.*)"/Uis', $html, $match);
        return isset($match[1]) ? $match[1] : '';
    }

    /**
     * @return string
     */
    private function getInterruptUser()
    {
        $file = Helper::LOG_FOLDER . static::INTERRUPT_USER;
        if (file_exists($file)) {
            $content = file_get_contents($file);
            if ($content) {
                $content = explode("\n", $content);
                if (isset($content[0])) {
                    return trim($content[0]);
                }
            }
        }
        return '';
    }

    /**
     * Add PID's into orders.order_info
     *
     * @param array $userOrders
     *
     * @return false|null
     * @throws \yii\db\Exception
     */
    public function addSN($userOrders)
    {
        if (!$this->accounts) return false;
        $url = 'https://shop.bitmain.com/user/viewOrderSn.htm?subId=';
        foreach ($this->accounts as $user) {
            $this->setTokens();
            if ($this->login($user)) {
                if (!isset($userOrders[$user['id']])) continue;
                foreach ($userOrders[$user['id']] as $bitOrderId) {
                    $html     = $this->getContent($url . $bitOrderId);
                    $document = phpQuery::newDocument($html);
                    $el       = $document->find('.table-view.table td')->not(':first');
                    $sn       = $el->html();
                    if ($sn) {
                        $sn  = str_replace(' ', '', $sn);
                        $sn = preg_replace('#(\w+\d+)#u', "'$1'", $sn);
                        $sql = "UPDATE orders SET info=JSON_SET(COALESCE(info, '{}'), '$.sn', JSON_ARRAY({$sn})) WHERE bitmain_order_id='{$bitOrderId}'";
                        Yii::$app->getDb()->createCommand($sql)->execute();
                    }
                }
            }
        }
    }
}