<?php

namespace app\modules\api\controllers;

use app\components\Helper;
use app\components\Parser;
use app\components\Selenium;
use app\modules\api\models\Accounts;
use app\modules\api\models\Orders;
use Yii;
use yii\base\InlineAction;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\Controller;
use yii\web\Request;
use yii\web\Response;

set_time_limit(3600);

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
    const EMAIL_ERROR = 1;
    const USER_ALREADY_EXISTS = 2;
    const EMPTY_PASSWORD = 3;
    const PARAM_IS_REQUIRED = 4;
    const ORDER_NOT_FOUND = 5;
    const SERVER_NOT_ANSWER = 6;
    const INVALID_COUNTRY = 7;

    public function init()
    {
        parent::init();
        Yii::$app->request->enableCsrfValidation = false;
        Yii::$app->request->enableCookieValidation = false;
    }

    /**
     * @param string $id
     *
     * @return null|object|\yii\base\Action|InlineAction
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function createAction($id)
    {
        if ($id === '') {
            $id = $this->defaultAction;
        }

        $actionMap = $this->actions();
        if (isset($actionMap[$id])) {
            return Yii::createObject($actionMap[$id], [$id, $this]);
        } elseif (preg_match('/^[A-Za-z0-9\\-_]+$/', $id) && strpos($id, '--') === false && trim($id, '-') === $id) {
            $methodName = 'action' . str_replace(' ', '', ucwords(implode(' ', explode('-', $id))));
            if (method_exists($this, $methodName)) {
                $method = new \ReflectionMethod($this, $methodName);
                if ($method->isPublic() && $method->getName() === $methodName) {
                    return new InlineAction($id, $this, $methodName);
                }
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::class
        ];
        $behaviors['verbs'] = [
        'class' => \yii\filters\VerbFilter::class,
        'actions' => [
            'index'  => ['get'],
            'userAdd'   => ['post'],
            'getOrder' => ['get'],
            'addToCart' => ['get'],
            'editUsersAddress' => ['post']
        ],
    ];
        return $behaviors;
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param array $params
     * @param bool $isJson
     *
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionUserAdd($params = [], $isJson = true)
    {
        $id = 0;
        if (isset($params['email'])) {
            $email = $params['email'];
        } else {
            $email = Yii::$app->request->post('email');
        }
        if (isset($params['pass'])) {
            $pass = $params['pass'];
        } else {
            $pass = Yii::$app->request->post('pass');
        }
        if (!$email) {
            return $this->showResult('Email not valid', static::EMAIL_ERROR);
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            if (!$pass) {
                return $this->showResult('Empty password', static::EMPTY_PASSWORD);
            }
            if (!Helper::checkEmailExists($_POST['email'])) {
                $accounts = new Accounts();
                $accounts->email = $email;
                $accounts->password = $pass;
                $accounts->active = 1;
                if ($accounts->save(true)) {
                    $id = $accounts->getPrimaryKey();
                }
            } else {
                return $this->showResult('User already exists', static::USER_ALREADY_EXISTS);
            }
        } else {
            return $this->showResult('Email not valid', static::EMAIL_ERROR);
        }
        return $this->showResult(['id' => $id]);
    }

    public function actionUserEdit()
    {
        $email = Yii::$app->request->post('email');
        $pass = Yii::$app->request->post('pass');
        $state = Yii::$app->request->post('state');
        $result = false;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->showResult('Email not valid', static::EMAIL_ERROR);
        }

        $account = Accounts::findOne(['email' => $email]);
        if ($account) {
            $account->password = $pass;
            if ($state !== null) {
                $account->active = $state;
            }
            $result = $account->save(true);
        }
        return $this->showResult($result);
    }

    /**
     *
     * @return array
     */
    public function actionGetUserOrders()
    {
        $email = Yii::$app->request->get('email');
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $startId = Yii::$app->request->get('startId');
            $where = "u.email='{$email}' ";
            if ($startId) {
                $where .= 'AND o.id >=' . $startId;
            }
            $orders = Orders::getOrders($where);
            return $this->showResult($orders);
        } else {
            return $this->showResult('Email not valid', static::EMAIL_ERROR);
        }
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionOrderSwitchState()
    {
        $orderId = Yii::$app->request->post('orderId');
        $state = Yii::$app->request->post('state');
        if (!$orderId) {
            return $this->showResult('OrderId is required', static::PARAM_IS_REQUIRED);
        }
        $sql = "UPDATE orders SET active={$state} WHERE id={$orderId}";
        $result = Yii::$app->getDb()->createCommand($sql)->execute();
        return $this->showResult(['result' => $result]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionUserSwitchState()
    {
        $userId = Yii::$app->request->post('userId');
        $state = Yii::$app->request->post('state');
        if (!$userId) {
            return $this->showResult('UserId is required', static::PARAM_IS_REQUIRED);
        }
        $sql = "UPDATE accounts SET active={$state} WHERE id={$userId}";
        $result = Yii::$app->getDb()->createCommand($sql)->execute();
        return $this->showResult(['result' => $result]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionGetOrder()
    {
        $bmOrderId = Yii::$app->request->get('bitmain_order_id');
        $email = Yii::$app->request->get('email');
        $pass = Yii::$app->request->get('pass');

        if (!$bmOrderId) {
            return $this->showResult('OrderId is required', static::PARAM_IS_REQUIRED);
        }
        if (!$email) {
            return $this->showResult('Email is required', static::PARAM_IS_REQUIRED);
        }
        if (!$pass) {
            return $this->showResult('Pass is required', static::PARAM_IS_REQUIRED);
        }

        $user = Helper::getUser(['email' => $email]);
        if (!$user) {
            $id = $this->actionUserAdd(['email' => $email, 'pass' => $pass], false);
        } else {
            $id = $user['id'];
        }

        $parser = new Parser([
            'email' => $email,
            'password' => $pass,
            'id' => $id
        ]);
        $orderId = $parser->getOrderById($bmOrderId);
        if ($orderId) {
            $where = 'o.id=' . $orderId;
            $order = Orders::getOrders($where);
            return $this->showResult($order);
        } else {
            return $this->showResult('Order not found', static::ORDER_NOT_FOUND);
        }
    }

    /**
     *
     * @throws \Facebook\WebDriver\Exception\NoSuchElementException
     * @throws \Facebook\WebDriver\Exception\TimeOutException
     * @throws \yii\db\Exception
     */
    public function actionAddToCart()
    {
        $pid = Yii::$app->request->get('pid');
        $num = Yii::$app->request->get('amount');
        if (!$pid) {
            return $this->showResult('pid is required', static::PARAM_IS_REQUIRED);
        }
        if (!$num) {
            return $this->showResult('amount is required', static::PARAM_IS_REQUIRED);
        }
        $parser = new Parser();
        $parser->curlInit();
        # get product info
        $url    = "https://shop-product.bitmain.com/api/product/getDetail?productId={$pid}&lang=en";
        $json = $parser->getContent($url);
        if (!$json) {
            return $this->showResult('Not answer from ' . $url, static::SERVER_NOT_ANSWER);
        }
        $productInfo = Helper::jsonParse($json);
        if ($num < $productInfo['buyMaxCount'] || $productInfo['buyMaxCount'] == 0) {
            $productInfo['buyMaxCount'] = $num;
        }
        $countUsers = $num / $productInfo['buyMaxCount'];
        $accounts = Accounts::getUsersForBuy($countUsers, $pid);
        $result = [];
        foreach ($accounts as $user) {
            if ($productInfo['productStatus'] == 1) {
                $url = "https://shop.bitmain.com/product/detail?pid={$pid}";
            } elseif ($productInfo['productStatus'] == 2) {
                $url = "https://shop.bitmain.com/product/add?pid={$pid}&num={$productInfo['buyMaxCount']}&action=add";
            }
            $selenium = new Selenium($url);
            $orderId = $selenium->checkout($productInfo, $user);
            if ($orderId) {
                $result[$user['email']] = $orderId;
                Helper::log('cart.log', $orderId);
            } else {
                Helper::log('cart.log', $pid . ' Not ordered ' . $json);
            }
        }
        #parse all orders in current accounts
        (new Parser($accounts))->start();
        $this->showResult($result);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionEditUsersAddress()
    {
        $data = [];
        $data['country'] = Yii::$app->request->post('country');
        $data['state_country'] = Yii::$app->request->post('state_country');
        $data['city'] = Yii::$app->request->post('city');
        $data['full_name'] = Yii::$app->request->post('full_name');
        $data['mobile'] = Yii::$app->request->post('mobile');
        $data['zip'] = Yii::$app->request->post('zip');
        $data['address'] = Yii::$app->request->post('address');
        $userId = Yii::$app->request->post('userId');

        if (!in_array($data['country'], Helper::getAvailableCountries())) {
            return $this->showResult('Invalid country', static::INVALID_COUNTRY);
        }

        if (!$userId) {
            return $this->showResult('userId is required', static::PARAM_IS_REQUIRED);
        }
        $where = '';
        if (is_array($userId)) {
            $where = 'WHERE id IN (' . implode(',', $userId) . ')';
        } elseif ($userId !== 'all') {
            $where = 'WHERE id=' . $userId;
        }
        $values = '';
        foreach ($data as $key => $val) {
            if (!$val) {
                return $this->showResult($key . ' is required', static::PARAM_IS_REQUIRED);
            }
            $values .= "{$key}='{$val}',";
        }

        $values = substr($values, 0, -1);

        $sql = "UPDATE accounts SET {$values} {$where}";
        $result = Yii::$app->getDb()->createCommand($sql)->execute();
        return $this->showResult(['result' => $result]);
    }

    /**
     * @return array
     * @throws \yii\db\Exception
     */
    public function actionGetProducts()
    {
        $result= Yii::$app->getDb()->createCommand('SELECT * FROM wares WHERE status=1 OR status=2')->queryAll();
        return $this->showResult($result);
    }

    /**
     * @param $data
     * @param int $errorCode
     * @param bool $isJson
     *
     * @return array
     */
    private function showResult($data, $errorCode = 0, $isJson = true)
    {
        if ($isJson) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return [
            'errorCode' => $errorCode,
            'data' => $data
        ];
    }

}
