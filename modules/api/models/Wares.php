<?php

namespace app\modules\api\models;

use Yii;

/**
 * This is the model class for table "wares".
 *
 * @property int $id
 * @property string $name
 * @property int $price
 * @property int $status
 * @property string $currency
 * @property string $product_id
 * @property string $description
 */
class Wares extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'wares';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'status', 'currency', 'product_id', 'description'], 'required'],
            [['price'], 'integer'],
            [['name', 'product_id'], 'string', 'max' => 45],
            [['status'], 'string', 'max' => 1],
            [['currency'], 'string', 'max' => 10],
            [['description'], 'string', 'max' => 128],
        ];
    }

    public static function getWaresByProductId($id)
    {
        if (is_array($id)) {
            $id = "'" . implode("','", $id) . "'";
            $sql = "SELECT * FROM wares WHERE product_id IN({$id})";
        } else {
            $sql = "SELECT * FROM wares WHERE product_id='{$id}'";
        }

        $result = Yii::$app->getDb()->createCommand($sql)->queryAll(\PDO::FETCH_ASSOC);

        $data = [];
        foreach ($result as $res) {
            $productId = $res['product_id'];
            unset($res['product_id']);
            $data[$productId] = $res;
        }
        $result = $data;

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'price' => Yii::t('app', 'Price'),
            'status' => Yii::t('app', 'Status'),
            'currency' => Yii::t('app', 'Currency'),
            'product_id' => Yii::t('app', 'Product ID'),
            'description' => Yii::t('app', 'Description'),
        ];
    }
}
