<?php

namespace app\modules\api\models;

use app\components\CounterTrait;
use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int $user_id
 * @property string $bitmain_order_id
 * @property string $recipient_phone
 * @property string $recipient_name
 * @property string $recipient_address
 * @property string $payment_total_amount
 * @property string $payment_received_amount
 * @property string $payment_discount
 * @property string $payment_rate_btc_usd
 * @property string $payment_rate_bch_usd
 * @property string $payment_rate_ltc_usd
 * @property string $payment_rate_usd_cny
 * @property string $payment_date
 * @property string $shipment_company
 * @property string $shipment_tracking_numbers
 * @property string $shipment_date
 * @property string $order_date
 * @property string $shipment_cost
 * @property int $date_create
 * @property int $date_update
 * @property array $info
 * @property int $active
 * @property array $order_location
 *
 * @property Accounts $user
 * @property Products[] $products
 */
class Orders extends \yii\db\ActiveRecord
{
    use CounterTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date_create'], 'required'],
            [['user_id', 'date_create', 'date_update'], 'integer'],
            [['recipient_address', 'info', 'order_location'], 'string'],
            [['bitmain_order_id', 'recipient_phone', 'recipient_name', 'payment_total_amount', 'payment_received_amount', 'payment_discount', 'payment_rate_btc_usd', 'payment_rate_bch_usd', 'payment_rate_ltc_usd', 'payment_rate_usd_cny', 'payment_date', 'shipment_company', 'shipment_tracking_numbers', 'shipment_date', 'order_date', 'shipment_cost'], 'string', 'max' => 45],
            //[['active'], 'string', 'max' => 1],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @param string $id
     *
     * @return Orders|array|null|\yii\db\ActiveRecord
     */
    public static function getOrderByBitmainId($id)
    {
        return static::find()->where(['bitmain_order_id' => $id])->one();
    }

    public static function getOrders($where)
    {
        $sql = "SELECT
                o.id,
                o.user_id,
                o.bitmain_order_id,
                o.recipient_phone,
                o.recipient_name,
                o.recipient_address,
                o.payment_total_amount,
                o.payment_received_amount,
                o.payment_discount,
                o.payment_rate_btc_usd,
                o.payment_rate_bch_usd,
                o.payment_rate_ltc_usd,
                o.payment_rate_usd_cny,
                o.payment_date,
                o.shipment_company,
                o.shipment_tracking_numbers,
                o.shipment_date,
                o.shipment_cost,
                o.date_create,
                o.order_date,
                o.order_location,
                o.date_update,
                GROUP_CONCAT(p.id) products
            FROM orders o
            JOIN accounts u ON o.user_id=u.id
            LEFT JOIN products p ON o.id=p.order_id
            WHERE {$where} AND o.active=1
            GROUP BY o.id
            ORDER BY o.id DESC";

        $orders = Yii::$app->getDb()->createCommand($sql)->queryAll();

        foreach ($orders as &$order) {
            if ($order['products']) {
                $sql = "SELECT * FROM products WHERE id IN ({$order['products']})";
                $order['products'] = Yii::$app->getDb()->createCommand($sql)->queryAll();
            }
        }
        return $orders;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'bitmain_order_id' => Yii::t('app', 'Bitmain Order ID'),
            'recipient_phone' => Yii::t('app', 'Recipient Phone'),
            'recipient_name' => Yii::t('app', 'Recipient Name'),
            'recipient_address' => Yii::t('app', 'Recipient Address'),
            'payment_total_amount' => Yii::t('app', 'Payment Total Amount'),
            'payment_received_amount' => Yii::t('app', 'Payment Received Amount'),
            'payment_discount' => Yii::t('app', 'Payment Discount'),
            'payment_rate_btc_usd' => Yii::t('app', 'Payment Rate Btc Usd'),
            'payment_rate_bch_usd' => Yii::t('app', 'Payment Rate Bch Usd'),
            'payment_rate_ltc_usd' => Yii::t('app', 'Payment Rate Ltc Usd'),
            'payment_rate_usd_cny' => Yii::t('app', 'Payment Rate Usd Cny'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'shipment_company' => Yii::t('app', 'Shipment Company'),
            'shipment_tracking_numbers' => Yii::t('app', 'Shipment Tracking Numbers'),
            'shipment_date' => Yii::t('app', 'Shipment Date'),
            'order_date' => Yii::t('app', 'Order Date'),
            'shipment_cost' => Yii::t('app', 'Shipment Cost'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'info' => Yii::t('app', 'Info'),
            'active' => Yii::t('app', 'Active'),
            'order_location' => Yii::t('app', 'Order Location'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Accounts::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::class, ['order_id' => 'id']);
    }
}
