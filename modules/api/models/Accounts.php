<?php

namespace app\modules\api\models;

use app\components\CounterTrait;
use Yii;

/**
 * This is the model class for table "accounts".
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property int $active
 * @property string $country
 * @property string $city
 * @property string $full_name
 * @property string $mobile
 * @property string $zip
 * @property string $address
 *
 * @property Orders[] $orders
 * @property Products[] $products
 */
class Accounts extends \yii\db\ActiveRecord
{
    use CounterTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password'], 'required'],
            [['country', 'address'], 'string'],
            [['email', 'password'], 'string', 'max' => 45],
            [['active'], 'integer', 'max' => 1],
            [['active'], 'default', 'value' => 1],
            [['city', 'full_name'], 'string', 'max' => 255],
            [['mobile'], 'string', 'max' => 15],
            [['zip'], 'string', 'max' => 32],
            [['email'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
            'active' => Yii::t('app', 'Active'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'full_name' => Yii::t('app', 'Full Name'),
            'mobile' => Yii::t('app', 'Mobile'),
            'zip' => Yii::t('app', 'Zip'),
            'address' => Yii::t('app', 'Address'),
        ];
    }

    /**
     * @param $countUsers
     * @param $pid
     *
     * @return array|\yii\db\Command
     * @throws \yii\db\Exception
     */
    public static function getUsersForBuy($countUsers, $pid)
    {
        #exclude users
        $sql = "SELECT u.id FROM products p
          JOIN orders o ON p.order_id=o.id AND o.order_date > DATE_SUB(NOW(), INTERVAL 8 day)
          JOIN accounts u ON o.user_id=u.id
        WHERE p.pid='{$pid}' AND u.active=1 GROUP BY u.id" ;
        #exclude users
        $users = Yii::$app->getDb()->createCommand($sql)->queryAll(\PDO::FETCH_COLUMN);
        $where = 'active=1';
        if ($users) {
            $where .= ' AND id NOT IN (' . implode(',', $users) . ')';
        }
        $sql = "SELECT * FROM accounts WHERE {$where} LIMIT {$countUsers}";
        return Yii::$app->getDb()->createCommand($sql)->queryAll();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::class, ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::class, ['user_id' => 'id']);
    }
}
