<?php

namespace app\modules\api\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\api\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `app\modules\api\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'date_create', 'date_update'], 'integer'],
            [
                [
                    'bitmain_order_id',
                    'recipient_phone',
                    'recipient_name',
                    'recipient_address',
                    'payment_total_amount',
                    'payment_received_amount',
                    'payment_discount',
                    'payment_rate_btc_usd',
                    'payment_rate_bch_usd',
                    'payment_rate_ltc_usd',
                    'payment_rate_usd_cny',
                    'payment_date',
                    'shipment_company',
                    'shipment_tracking_numbers',
                    'shipment_date',
                    'order_date',
                    'shipment_cost',
                    'info',
                    'active',
                    'order_location'
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find()->with(['user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if ( ! $this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'user_id'     => $this->user_id,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'bitmain_order_id', $this->bitmain_order_id])
              ->andFilterWhere(['like', 'recipient_phone', $this->recipient_phone])
              ->andFilterWhere(['like', 'recipient_name', $this->recipient_name])
              ->andFilterWhere(['like', 'recipient_address', $this->recipient_address])
              ->andFilterWhere(['like', 'payment_total_amount', $this->payment_total_amount])
              ->andFilterWhere(['like', 'payment_received_amount', $this->payment_received_amount])
              ->andFilterWhere(['like', 'payment_discount', $this->payment_discount])
              ->andFilterWhere(['like', 'payment_rate_btc_usd', $this->payment_rate_btc_usd])
              ->andFilterWhere(['like', 'payment_rate_bch_usd', $this->payment_rate_bch_usd])
              ->andFilterWhere(['like', 'payment_rate_ltc_usd', $this->payment_rate_ltc_usd])
              ->andFilterWhere(['like', 'payment_rate_usd_cny', $this->payment_rate_usd_cny])
              ->andFilterWhere(['like', 'payment_date', $this->payment_date])
              ->andFilterWhere(['like', 'shipment_company', $this->shipment_company])
              ->andFilterWhere(['like', 'shipment_tracking_numbers', $this->shipment_tracking_numbers])
              ->andFilterWhere(['like', 'shipment_date', $this->shipment_date])
              ->andFilterWhere(['like', 'order_date', $this->order_date])
              ->andFilterWhere(['like', 'shipment_cost', $this->shipment_cost])
              ->andFilterWhere(['like', 'info', $this->info])
              ->andFilterWhere(['like', 'active', $this->active])
              ->andFilterWhere(['like', 'order_location', $this->order_location]);

        return $dataProvider;
    }
}
