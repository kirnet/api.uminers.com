<?php

namespace app\modules\api\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\api\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `app\modules\api\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'user_id', 'product_count', 'product_price', 'date_create', 'date_update'], 'integer'],
            [['product_title', 'product_currency', 'product_courier', 'product_shipping_cost', 'product_shipping_date', 'pid', 'active'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'user_id' => $this->user_id,
            'product_count' => $this->product_count,
            'product_price' => $this->product_price,
            'date_create' => $this->date_create,
            'date_update' => $this->date_update,
        ]);

        $query->andFilterWhere(['like', 'product_title', $this->product_title])
            ->andFilterWhere(['like', 'product_currency', $this->product_currency])
            ->andFilterWhere(['like', 'product_courier', $this->product_courier])
            ->andFilterWhere(['like', 'product_shipping_cost', $this->product_shipping_cost])
            ->andFilterWhere(['like', 'product_shipping_date', $this->product_shipping_date])
            ->andFilterWhere(['like', 'pid', $this->pid])
            ->andFilterWhere(['like', 'active', $this->active]);

        return $dataProvider;
    }
}
