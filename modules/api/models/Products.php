<?php

namespace app\modules\api\models;

use app\components\CounterTrait;
use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $product_title
 * @property int $product_count
 * @property int $product_price
 * @property string $product_currency
 * @property string $product_courier
 * @property string $product_shipping_cost
 * @property string $product_shipping_date
 * @property int $date_create
 * @property int $date_update
 * @property string $pid
 * @property int $active
 *
 * @property Orders $order
 * @property Accounts $user
 */
class Products extends \yii\db\ActiveRecord
{
    use CounterTrait;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'pid'], 'required'],
            [['order_id', 'user_id', 'product_count', 'product_price', 'date_create', 'date_update'], 'integer'],
            [['product_title', 'product_courier'], 'string', 'max' => 255],
            [['product_currency', 'product_shipping_cost', 'product_shipping_date', 'pid'], 'string', 'max' => 45],
            [['active'], 'string', 'max' => 1],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::class, 'targetAttribute' => ['order_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'product_title' => Yii::t('app', 'Product Title'),
            'product_count' => Yii::t('app', 'Product Count'),
            'product_price' => Yii::t('app', 'Product Price'),
            'product_currency' => Yii::t('app', 'Product Currency'),
            'product_courier' => Yii::t('app', 'Product Courier'),
            'product_shipping_cost' => Yii::t('app', 'Product Shipping Cost'),
            'product_shipping_date' => Yii::t('app', 'Product Shipping Date'),
            'date_create' => Yii::t('app', 'Date Create'),
            'date_update' => Yii::t('app', 'Date Update'),
            'pid' => Yii::t('app', 'Pid'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @param $data
     * @param $orderId
     *
     * @return mixed
     * @throws \yii\db\Exception
     */
    public static function filterProductsExists($data, $orderId)
    {
        $pids = array_keys($data);
        if ($pids) {
            $pids = "'" . implode("','", $pids) . "'";

            $sql = "SELECT pid FROM products WHERE pid IN({$pids}) AND order_id={$orderId}";
//            $st = App::$db->query($sql);
            $products = Yii::$app->getDb()->createCommand($sql)->queryAll();
//            $products = $st->fetchAll(\PDO::FETCH_ASSOC);
            foreach ($products as $val) {
                unset($data[$val['pid']]);
            }
        }
        return $data;
    }

    public static function getById($id)
    {
        return static::find()->where(['id' => $id])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::class, ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Accounts::class, ['id' => 'user_id']);
    }
}
