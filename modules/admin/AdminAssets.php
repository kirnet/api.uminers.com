<?php
/**
 * Created by PhpStorm.
 * User: kirnet
 * Date: 14.04.18
 * Time: 2:03
 */

namespace app\modules\admin;


use yii\web\AssetBundle;

class AdminAssets extends AssetBundle
{
    public $css = [
        //"https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"
    ];
}