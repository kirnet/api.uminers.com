<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\api\models\Orders */

?>
<div class="orders-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
