<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'bitmain_order_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipient_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipient_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'recipient_address')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'payment_total_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_received_amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_discount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_rate_btc_usd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_rate_bch_usd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_rate_ltc_usd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_rate_usd_cny')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'payment_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipment_company')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipment_tracking_numbers')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipment_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'shipment_cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_create')->textInput() ?>

    <?= $form->field($model, 'date_update')->textInput() ?>

    <?php //= $form->field($model, 'info')->textInput() ?>

    <?php //= $form->field($model, 'order_location')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
