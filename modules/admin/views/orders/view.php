<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\Orders */
?>
<div class="orders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'bitmain_order_id',
            'recipient_phone',
            'recipient_name',
            'recipient_address:ntext',
            'payment_total_amount',
            'payment_received_amount',
            'payment_discount',
            'payment_rate_btc_usd',
            'payment_rate_bch_usd',
            'payment_rate_ltc_usd',
            'payment_rate_usd_cny',
            'payment_date',
            'shipment_company',
            'shipment_tracking_numbers',
            'shipment_date',
            'order_date',
            'shipment_cost',
            'date_create',
            'date_update',
            [
                'label' => 'info',
                'value' => function($model) {
                    return json_encode($model->info);
                }
            ],
            'active',
            [
                'label' => 'order_location',
                'value' => function($model) {
                    return json_encode($model->order_location);
                }
            ]
        ],
    ]) ?>

</div>
