<?php

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'id',
    ],
    [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'user_id',
        'value' => function($model, $key, $index, $widget) {
            return $model->user->email;
        },
        'filterType' => GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(\app\modules\api\models\Accounts::find()->orderBy('email')->asArray()->all(), 'id', 'email'),
        'filterWidgetOptions' => [
            'pluginOptions' => ['allowClear' => true],
        ],
        'filterInputOptions' => ['placeholder' => 'Any account'],
        'format' => 'raw'
    ],
    [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'bitmain_order_id',
    ],
    [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'recipient_phone',
    ],
    [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'recipient_name',
    ],
//    [
//        'class'     => '\kartik\grid\DataColumn',
//        'attribute' => 'recipient_address',
//    ],
     [
     'class'=>'\kartik\grid\DataColumn',
     'attribute'=>'payment_total_amount',
     ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_received_amount',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_discount',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_rate_btc_usd',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_rate_bch_usd',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_rate_ltc_usd',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_rate_usd_cny',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'payment_date',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'shipment_company',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'shipment_tracking_numbers',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'shipment_date',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'order_date',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'shipment_cost',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'date_create',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'date_update',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'info',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'active',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'order_location',
    // ],
    [
        'class'         => 'kartik\grid\ActionColumn',
        'dropdown'      => false,
        'vAlign'        => 'middle',
        'urlCreator'    => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions'   => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role'                 => 'modal-remote',
            'title'                => 'Delete',
            'data-confirm'         => false,
            'data-method'          => false,// for overide yii data api
            'data-request-method'  => 'post',
            'data-toggle'          => 'tooltip',
            'data-confirm-title'   => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   