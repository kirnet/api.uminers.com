<?php

use app\components\Helper;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'email',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'password',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'active',
        'value' => function($model) {
            $yesNo = Helper::YesNo();
            return $yesNo[$model->active];
        },
        'filter' => Helper::YesNo()
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'country',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'city',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'full_name',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'mobile',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'zip',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'address',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions' => ['role'=>'modal-remote','title' => 'View','data-toggle'=>'tooltip'],
        'updateOptions' => ['role'=>'modal-remote','title' => 'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions' => ['role'=>'modal-remote','title' => 'Delete',
          'data-confirm' => false, 'data-method' => false,// for overide yii data api
          'data-request-method' => 'post',
          'data-toggle' => 'tooltip',
          'data-confirm-title' => 'Are you sure?',
          'data-confirm-message' => 'Are you sure want to delete this item'],
    ],

];   