<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\Accounts */
?>
<div class="accounts-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'email:email',
            'password',
            'active',
            'country',
            'city',
            'full_name',
            'mobile',
            'zip',
            'address:ntext',
        ],
    ]) ?>

</div>
