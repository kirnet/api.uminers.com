<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\Products */
?>
<div class="products-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'user_id',
            'product_title',
            'product_count',
            'product_price',
            'product_currency',
            'product_courier',
            'product_shipping_cost',
            'product_shipping_date',
            'date_create',
            'date_update',
            'pid',
            'active',
        ],
    ]) ?>

</div>
