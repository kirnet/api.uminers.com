<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $changePassword null|static */

$this->title = Yii::t('app', 'Profile');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-changePassword">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($changePassword, 'oldPassword')->passwordInput() ?>
    <?= $form->field($changePassword, 'newPassword')->passwordInput() ?>
    <?= $form->field($changePassword, 'confirmNewPassword')->passwordInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app','Change'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>