<div class="admin-default-index">
<table class="table">
    <tr>
        <th><?= Yii::t('app', 'Title')?></th>
        <th><?= Yii::t('app', 'All')?></th>
        <th><?= Yii::t('app', 'Not Active')?></th>
    </tr>
    <?php foreach ($counters as $title => $val): ?>
    <tr>
        <td><?= $title ?></td>
        <td><?= $val['countAccounts'] ?></td>
        <td><?= $val['countNotActiveAccounts'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>

</div>
