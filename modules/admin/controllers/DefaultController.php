<?php

namespace app\modules\admin\controllers;

use app\components\CounterTrait;
use app\modules\api\models\Accounts;
use app\modules\api\models\Orders;
use app\modules\api\models\Products;
use app\modules\user\models\ChangePasswordForm;
use app\modules\user\models\User;
use Yii;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $counters = [];
        $counters[Yii::t('app', 'Accounts')]['countAccounts'] = Accounts::countAllItems();
        $counters[Yii::t('app', 'Accounts')]['countNotActiveAccounts'] = Accounts::countNotActiveItems();

        $counters[Yii::t('app', 'Orders')]['countAccounts'] = Orders::countAllItems();
        $counters[Yii::t('app', 'Orders')]['countNotActiveAccounts'] = Orders::countNotActiveItems();

        $counters[Yii::t('app', 'Products')]['countAccounts'] = Products::countAllItems();
        $counters[Yii::t('app', 'Products')]['countNotActiveAccounts'] = Products::countNotActiveItems();

        return $this->render('index', ['counters' => $counters]);
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function actionProfile()
    {
        $changePassword = new ChangePasswordForm;

        if ($changePassword->load(Yii::$app->getRequest()->post()) && $changePassword->change()) {
            Yii::$app->session->setFlash('success', 'Password Changed!');
        }

        return $this->render('profile', ['changePassword' => $changePassword]);
    }
}
