<?php

namespace app\modules\user\models;

use Yii;
use yii\base\Model;
use app\modules\user\models\User;

class ChangePasswordForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $confirmNewPassword;

    public function rules()
    {
        return [
            [['oldPassword', 'newPassword', 'confirmNewPassword'], 'required'],
            [['oldPassword'], 'validatePassword'],
            [['newPassword'], 'string', 'min' => 6],
            [['confirmNewPassword'], 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     */
    public function validatePassword()
    {
        /** @var  $user User */
        $user = Yii::$app->user->identity;
        if (!$user || !$user->validatePassword($this->oldPassword)) {
            $this->addError('oldPassword', Yii::t('app','Incorrect old password.'));
        }
    }

    /**
     * Change password.
     *
     * @param int $id
     * @return bool the saved model or null if saving fails
     * @throws \yii\base\Exception
     */
    public function change($id = 0)
    {
        if ($this->validate()) {
            if ($id) {
                $user = User::findOne($id);
            } else {
                /* @var $user User */
                $user = Yii::$app->user->identity;
            }
            $user->setPassword($this->newPassword);
            if ($user->save()) {
                return true;
            }
        }
        return false;
    }

    public function attributeLabels()
    {
        return [
            'oldPassword'        => Yii::t('app', 'Old Password'),
            'newPassword'        => Yii::t('app', 'New Password'),
            'confirmNewPassword' => Yii::t('app', 'Repeat New Password'),
        ];
    }
}